import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

public class JPanelB extends JPanel {

	Main mainFrame;
	JTable table;

	public JTable getTable() {
		return table;
	}

	class Column0Renderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			setBackground(table.getTableHeader().getBackground());
			setValue(new Integer(row + 1));
			setBorder(new BevelBorder(BevelBorder.RAISED));
			return (this);
		}
	}

	class HeaderRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (column == 0) {
				setBackground(Color.white);
				setText("");
			} else {
				char columnSymbol = (char) (column + '@');
				setText(String.valueOf(columnSymbol));
				setHorizontalAlignment(SwingConstants.CENTER);
				setBorder(new BevelBorder(BevelBorder.RAISED));
			}
			return (this);
		}
	}

	public JPanelB(Main main) {

		// JTableの生成
		table = new JTable(20, 7);
		JScrollPane scroll = new JScrollPane(table);
		this.add(scroll);
		// JTableHeader header = table.getTableHeader();
		// this.add(header, BorderLayout.NORTH);
		// this.add(table);

		DefaultTableColumnModel cmodel = (DefaultTableColumnModel) table
				.getColumnModel();
		for (int i = 0; i < table.getColumnCount(); i++) {
			TableColumn column = cmodel.getColumn(i);
			if (i == 0) {
				column.setCellRenderer(new Column0Renderer());
				column.setMaxWidth(40);
			}
			column.setHeaderRenderer(new HeaderRenderer());
		}
		// パネルの設定
		// this.setBorder(new LineBorder(Color.black));
	}

}
