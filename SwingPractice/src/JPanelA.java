import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class JPanelA extends JPanel implements ActionListener {

	Main mainFrame;
	JComboBox combo;

	final String SHOW_PANEL_B = "showPanelB";
	final String HIDE_PANEL_B = "hidePanelB";

	public JPanelA(Main main) {
		mainFrame = main;

		// レイアウトの指定
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);

		// 背景色を指定
		this.setBackground(Color.red);

		// ボタンの追加
		JButton buttonShow = new JButton("表示");
		JButton buttonHide = new JButton("非表示");

		buttonShow.setPreferredSize(new Dimension(75, 25));
		buttonShow.addActionListener(this);
		buttonShow.setActionCommand(SHOW_PANEL_B);
		buttonHide.setPreferredSize(new Dimension(75, 25));
		buttonHide.addActionListener(this);
		buttonHide.setActionCommand(HIDE_PANEL_B);

		layout.putConstraint(SpringLayout.NORTH, buttonShow, 10,
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, buttonShow, 20,
				SpringLayout.WEST, this);

		layout.putConstraint(SpringLayout.NORTH, buttonHide, 10,
				SpringLayout.SOUTH, buttonShow);
		layout.putConstraint(SpringLayout.WEST, buttonHide, 20,
				SpringLayout.WEST, this);

		this.add(buttonShow);
		this.add(buttonHide);

		// コンボボックスの追加
		String[] choice = { "赤", "青", "黄" }; // コンボボックスの選択肢
		combo = new JComboBox(choice);
		combo.setPreferredSize(new Dimension(125, 25));
		combo.addActionListener(this);

		layout.putConstraint(SpringLayout.NORTH, combo, 10, SpringLayout.NORTH,
				this);
		layout.putConstraint(SpringLayout.WEST, combo, 150, SpringLayout.WEST,
				this);

		this.add(combo, BorderLayout.NORTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// 表示・非表示ボタンが押された時の処理
		String cmd = e.getActionCommand();
		if (cmd.equals(SHOW_PANEL_B)) {
			mainFrame.panelB.setVisible(true);
		} else if (cmd.equals(HIDE_PANEL_B)) {
			mainFrame.panelB.setVisible(false);
		}

		// コンボボックスで選択された項目によって背景色を変更する
		String chosenColor = (String) combo.getSelectedItem();
		if (chosenColor.equals("赤")) {
			this.setBackground(Color.red);
		} else if (chosenColor.equals("青")) {
			this.setBackground(Color.blue);
		} else if (chosenColor.equals("黄")) {
			this.setBackground(Color.yellow);
		} else {
			System.out.println("未対応のchosenColor:" + chosenColor);
		}
	}
}
