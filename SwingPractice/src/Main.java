import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Main extends JFrame {

	JPanelA panelA; // 上部パネル
	JPanelB panelB; // 中部パネル
	JPanelC panelC; // 下部パネル

	public static void main(String[] args) {
		Main frame = new Main("Swing練習用画面");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);// ウィンドウサイズ
		frame.setLocationRelativeTo(null);// ウィンドウを画面中央に表示
		frame.setVisible(true);
	}

	Main(String title) {
		super(title);
		BorderLayout layout = new BorderLayout();
		Container pane = getContentPane();
		pane.setLayout(layout);
		layout.setVgap(10);// コンポーネント間の間隔
		layout.setHgap(10);

		// 各パネルにこのクラスのインスタンスを持たせることで、パネルから別のパネルへアクセス可能にした
		panelA = new JPanelA(this);
		panelA.setPreferredSize(new Dimension(790, 90));
		// panelA.setBorder(border);
		pane.add(panelA, BorderLayout.NORTH);

		panelB = new JPanelB(this);
		panelB.setPreferredSize(new Dimension(790, 390));
		pane.add(panelB, BorderLayout.CENTER);

		panelC = new JPanelC(this);
		panelC.setPreferredSize(new Dimension(790, 90));
		pane.add(panelC, BorderLayout.SOUTH);
	}
}
